# Docker Configuration



## Getting started

This configuration/files is for when you can dockerizar a full app (Backend(NestJS) + Frontend (Angular) + Database (MySql) + PhpMyAdmin)

## DOCKER COMPOSE

When I wanna run the complete-db.sql into database, there is an error for ```sql DEFAULT uuid()```, so we need to replace those text for empty.
Other solution is change the image, and  test all app.

Acoording the project WOW, is necessary run migration.sql, and all views
### database
```yaml
 database:
    image: mariadb:10.6 # Image for database
    networks:
      - net_wow #network for the docker
    ports: 
      - 3306:3306 # host:container
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: ${DATABASE_PWD} #see the .env file
      MYSQL_DATABASE: ${DATABASE_NAME} #see the .env file
    volumes:
      - database:/var/lib/mysql # volumen for database
      - ./backend/DB/complete-db.sql:/docker-entrypoint-initdb.d/init.sql #always init with 01.sql file into backend/DB/01.sql
```

### PhpMyAdmin

```yaml
  phpmyadmin:
    image: phpmyadmin
    ports: 
      - 8080:80 # localhost: container
    networks:
      - net_wow
    depends_on:
      - database # depends of database service
    environment:
      PMA_HOST: database
      PMA_PORT: 3306
      UPLOAD_LIMIT: 300M #max size for import files
```

### Frontend
In the file enviroment.production is necessary  change the host to localhost
```yaml
  web:
    build:
      context: ./frontend #this folder can be change for your foldername
      dockerfile: frontend.dockerfile
    restart: always    
    ports:
      - 80:80
    networks:
      - net_wow
```


### Backend
The DATABASE_URL is variable into ./backend/.env where is the connection between PRISMA and MySql.

```yaml
  api:
    build:
      context: ./backend #this folder can be change for your foldername
      dockerfile: backend.dockerfile
    restart: always        
    depends_on:
      - database
    env_file:
      - .env
    environment:
      DATABASE_URL: "mysql://${DATABASE_USER}:${DATABASE_PWD}@${DATABASE_HOST}:${DATABASE_PORT}/${DATABASE_NAME}"
    ports:
      - 3000:3000    
    networks:
      - net_wow
```

In the backend maybe is necesary change the order into variables, for that
```js
DATABASE_HOST="localhost"
DATABASE_USER="root"
DATABASE_PWD="sample"
DATABASE_PORT="3306"
DATABASE_NAME="wow"
DATABASE_URL="mysql://${DATABASE_USER}:${DATABASE_PWD}@${DATABASE_HOST}:${DATABASE_PORT}/${DATABASE_NAME}"
```

## DOCKER COMMANDS

For init docker in the repo
```sh
docker-compose up --build -d
```
This command stop and remove volumens for dockers container.
```sh
docker-compose down -v
```

NOTE. Sometimes is necesary can do the SUDO in the command line.

## Contribution
Maybe you can improve the dockerfiles or docker-compose, you can FORK the repo and your commit.

